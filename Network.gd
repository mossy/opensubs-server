extends Node

# Config file variables

var ConfigLoader = load("res://Scripts/ConfigLoader.gd").new()

# Game variables

var Games : Dictionary  = {}

# Inbuilt functions

func _ready() -> void:
	randomize()
	ConfigLoader.load_config()
	print("Loaded config file")
	var NetworkENet := ENetMultiplayerPeer.new()
	NetworkENet.create_server(ConfigLoader.Port, ConfigLoader.MaximumConnectedClients)
	print("Listening on port " + str(ConfigLoader.Port))
	multiplayer.multiplayer_peer = NetworkENet
	multiplayer.connect("peer_connected", Callable(self, "peer_connected"))
	multiplayer.connect("peer_disconnected", Callable(self, "peer_disconnected"))

# Variable functions

# Returns the location of the save file
func get_save_path() -> String:
	return OS.get_executable_path().get_base_dir() + "/Server.save"

# Combines two dictionaries
func combine_dictionaries(a : Dictionary, b : Dictionary) -> void:
	for key in b.keys():
		a[key] = b[key]

# Returns complete information about a game
func get_complete_game_info(gameid : int) -> Dictionary:
	var GameInfo : Dictionary
	var GameData : Dictionary = Games[gameid]
	# Set all game setup variables
	var IgnoredVariables : Array = [
		"StartTime",
		"Orders",
		"Chats"
	]
	for variable in GameData:
		if IgnoredVariables.has(variable) == false:
			GameInfo[variable] = GameData[variable]
	return GameInfo

# Return basic information about a game
func get_basic_game_info(gameid : int) -> Dictionary:
	var GameInfo : Dictionary = get_complete_game_info(gameid)
	var BasicGameInfo : Dictionary = {
		"GameName" : GameInfo["GameName"],
		"PlayerCount" : GameInfo["PlayerCount"],
		"Users" : GameInfo["Users"]
	}
	return BasicGameInfo

# Returns all orders known to user
func get_known_orders(gameid : int, user : String, globaltick : int) -> Dictionary:
	var GameData : Dictionary = Games[gameid]
	var KnownOrders : Dictionary
	for ordertick in GameData["Orders"]:
		if ordertick <= globaltick:
			# Add order if in past
			KnownOrders[ordertick] = GameData["Orders"][ordertick]
		else:
			# Add order if in future and belongs to user
			for orderid in GameData["Orders"][ordertick]:
				if GameData["Orders"][ordertick][orderid]["User"] == user:
					if KnownOrders.has(ordertick) == false:
						KnownOrders[ordertick] = {}
					KnownOrders[ordertick][orderid] = GameData["Orders"][ordertick][orderid]
	return KnownOrders

# Returns a game's current global tick
func get_global_tick(gameid : int) -> int:
	var GameData : Dictionary = Games[gameid]
	var GlobalTick : int = 0
	if GameData["StartTime"] != -1:
		GlobalTick = int((Time.get_unix_time_from_system() - GameData["StartTime"]) / GameData["TickLength"])
	return GlobalTick

# Connection functions

# Called when peer connects to server
func peer_connected(id : int) -> void:
	print(str(id) + " connected")

# Called when peer disconnects from server
func peer_disconnected(id) -> void:
	print(str(id) + " disconnected")
	# Removes user from all users variables
	for gameid in Games:
		var User : String = get_online_user(gameid, id)
		if User != "":
			close_game(gameid, id, User)

# Returns user for client id 
func get_online_user(gameid : int, id : int) -> String:
	var GameData : Dictionary = Games[gameid]
	for onlineuser in GameData["OnlineUsers"]:
		if GameData["OnlineUsers"][onlineuser].has(id):
			return onlineuser
	# Failure case
	return ""

# Checks if passed user is present in game
func get_game_user_present(gameid : int, user : String) -> bool:
	var GameData : Dictionary = Games[gameid]
	return GameData["Users"].has(user)

# Returns the IDs of all online users
func get_online_user_ids(gameid : int) -> Array:
	var GameData : Dictionary = Games[gameid]
	var Ids : Array = []
	for user in GameData["OnlineUsers"]:
		Ids.append_array(GameData["OnlineUsers"][user])
	return Ids

# Order functions

# Sets order
@rpc("any_peer")
func set_server_order(gameid : int, id : int, order : Dictionary) -> void:
	var GameData : Dictionary = Games[gameid]
	# Set order in orders
	if GameData["Orders"].has(order["OrderTick"]) == false:
		GameData["Orders"][order["OrderTick"]] = {}
	GameData["Orders"][order["OrderTick"]][order["OrderId"]] = order
	# Set order for all other users
	var GlobalTick : int = get_global_tick(gameid)
	if order["OrderTick"] <= GlobalTick:
		for userid in get_online_user_ids(gameid):
			if userid != id:
				rpc_id(userid, "set_order", order)

# Removes order
@rpc("any_peer")
func remove_server_order(gameid : int, id : int, ordertick : int, orderid : int) -> void:
	var GameData : Dictionary = Games[gameid]
	# Remove order from orders
	GameData["Orders"][ordertick].erase(orderid)
	if GameData["Orders"][ordertick].is_empty():
		GameData["Orders"].erase(ordertick)
	# Remove order for all other users
	var GlobalTick : int = get_global_tick(gameid)
	if ordertick <= GlobalTick:
		for userid in get_online_user_ids(gameid):
			if userid != id:
				rpc_id(userid, "remove_order", ordertick, orderid)

# Sets create submarine order initial troops
@rpc("any_peer")
func set_server_create_submarine_order_initial_troops(gameid : int, id : int, ordertick : int, orderid : int, troops : int) -> void:
	var GameData : Dictionary = Games[gameid]
	# Set create submarine order initial troops in orders 
	var Order : Dictionary = GameData["Orders"][ordertick][orderid]
	Order["InitialTroops"] = troops
	# Set create submarine order troops for all other users
	var GlobalTick : int = get_global_tick(gameid)
	if ordertick <= GlobalTick:
		for userid in get_online_user_ids(gameid):
			if userid != id: 
				rpc_id(userid, "set_create_submarine_order_initial_troops", ordertick, orderid, troops)

# Returns all orders revealed to a user at the passed tick
@rpc("any_peer")
func get_revealed_orders(gameid : int, id : int, user : String, tick : int) -> void:
	var GameData : Dictionary = Games[gameid]
	var RevealedOrders : Array
	if GameData["Orders"].has(tick):
		for orderid in GameData["Orders"][tick]:
			if GameData["Orders"][tick][orderid]["User"] != user:
				RevealedOrders.append(GameData["Orders"][tick][orderid])
	if RevealedOrders.is_empty() == false:
		rpc_id(id, "set_revealed_orders", RevealedOrders)

# Game functions

# Creates a new game
@rpc("any_peer")
func create_game(id : int, user : String, gameinfo : Dictionary) -> void:
	# Get game info
	var NewGame = {
		"StartTime" : -1,
		"Users" : [],
		"OnlineUsers" : {},
		"Orders" : {},
		"Chats" : {}
	}
	combine_dictionaries(NewGame, gameinfo)
	var GameId : int = randi()
	# Set game info
	Games[GameId] = NewGame
	var SaveFile = FileAccess.open(get_save_path(), FileAccess.WRITE)
	print("User " + user + " created game " + gameinfo["GameName"])
	# Client join game
	join_game(GameId, id, user)

# Open game
@rpc("any_peer")
func open_game(gameid : int, id : int, user : String) -> void:
	var GameData : Dictionary = Games[gameid]
	var GlobalTick : int = get_global_tick(gameid)
	print("User " + user + " opened game " + GameData["GameName"])
	# Set online user in online users
	if GameData["OnlineUsers"].has(user) == false:
		GameData["OnlineUsers"][user] = []
	# Set user id from online user
	GameData["OnlineUsers"][user].append(id)
	# Gets all orders known to a user
	var KnownOrders : Dictionary = get_known_orders(gameid, user, GlobalTick)
	# Gets all chats known to a user
	var KnownChats : Dictionary
	for chat in GameData["Chats"]:
		if chat.has(user):
			KnownChats[chat] = GameData["Chats"][chat]
	# Get game info
	var GameInfo : Dictionary = GameData.duplicate(true)
	GameInfo["Orders"] = KnownOrders
	GameInfo["Chats"] = KnownChats
	for key in GameInfo.keys():
		rpc_id(id, "set_game_variable", key, GameInfo[key])
	rpc_id(id, "change_to_game_scene")
	# Set online users
	for userid in get_online_user_ids(gameid):
		if userid != id:
			rpc_id(userid, "set_game_variable", "OnlineUsers", GameData["OnlineUsers"])

# Close game
@rpc("any_peer")
func close_game(gameid : int, id : int, user : String) -> void:
	var GameData : Dictionary = Games[gameid]
	print("User " + user + " closed game " + GameData["GameName"])
	# Remove user id from online user
	GameData["OnlineUsers"][user].erase(id)
	# Remove online user from online users
	if GameData["OnlineUsers"][user].is_empty():
		GameData["OnlineUsers"].erase(user)
	# Set online users
	for userid in get_online_user_ids(gameid):
		if userid != id:
			rpc_id(userid, "set_game_variable", "OnlineUsers", GameData["OnlineUsers"])

@rpc("any_peer")
func get_game_info(gameid : int, id : int) -> void:
	var GameInfo : Dictionary = get_complete_game_info(gameid)
	rpc_id(id, "set_game_info", GameInfo)

# Returns the games a user is present in
@rpc("any_peer")
func get_joined_games(id : int, user : String) -> void:
	var GamesInfo : Dictionary = {}
	for gameid in Games:
		if get_game_user_present(gameid, user):
			GamesInfo[gameid] = get_basic_game_info(gameid)
	rpc_id(id, "set_joined_games", GamesInfo)

# Returns all public games a user is not present in
@rpc("any_peer")
func get_public_games(id : int, user : String) -> void:
	var GamesInfo : Dictionary = {}
	for gameid in Games:
		if get_game_user_present(gameid, user) == false:
			GamesInfo[gameid] = get_basic_game_info(gameid)
	rpc_id(id, "set_public_games", GamesInfo)

# Checks if a user can join a game and adds them to it
@rpc("any_peer")
func join_game(gameid : int, id : int, user : String) -> void:
	var GameData : Dictionary = Games[gameid]
	if len(GameData["Users"]) < GameData["PlayerCount"]:
		if get_game_user_present(gameid, user) == false:
			# Add user to game
			GameData["Users"].append(user)
			# Start game
			var StartGame : bool = len(GameData["Users"]) == GameData["PlayerCount"]
			if StartGame:
				GameData["StartTime"] = Time.get_unix_time_from_system()
			for userid in get_online_user_ids(gameid):
				if userid != id:
					# Set users
					rpc_id(userid, "set_game_variable", "Users", GameData["Users"])
					# Set start time
					if StartGame:
						rpc_id(userid, "set_game_variable", "StartTime", GameData["StartTime"])
			# Update user joined games
			get_joined_games(id, user)
		print("User " + user + " joined game " + GameData["GameName"])

# Communication functions

# Sends a message
@rpc("any_peer")
func send_message(gameid : int, id : int, users : Array, message : Dictionary) -> void:
	var GameData : Dictionary = Games[gameid]
	# Set users in chats
	if GameData["Chats"].has(users) == false:
		GameData["Chats"][users] = []
	# Add message to chat
	GameData["Chats"][users].append(message)
	# Add message to online users chat
	for user in users:
		for userid in GameData["OnlineUsers"].get(user, []):
			if userid != id:
				rpc_id(userid, "set_message", users, message)

# Client main menu functions

@rpc("authority", "call_remote")
func set_joined_games(games : Dictionary) -> void:
	return

@rpc("authority", "call_remote")
func set_public_games(games : Dictionary) -> void:
	return

@rpc("authority", "call_remote")
func set_game_info(gameinfo : Dictionary) -> void:
	return

@rpc("authority", "call_remote")
func user_approved() -> void:
	return

@rpc("authority", "call_remote")
func user_rejected() -> void:
	return

@rpc("authority", "call_remote")
func change_to_game_scene() -> void:
	return

# Client variable functions

@rpc("authority", "call_remote")
func set_game_variable(variable : String, value) -> void:
	return

# Client order functions

@rpc("authority", "call_remote")
func set_order(order : Dictionary) -> void:
	return

@rpc("authority", "call_remote")
func remove_order(ordertype : String, orderid : int) -> void:
	return

@rpc("authority", "call_remote")
func set_revealed_orders(revealedorders : Array) -> void:
	return

@rpc("authority", "call_remote")
func set_create_submarine_order_initial_troops(ordertick : int, orderid : int, troops : int) -> void:
	return

# Client communication functions

@rpc("authority", "call_remote")
func set_message(users : Array, message : Dictionary) -> void:
	return
